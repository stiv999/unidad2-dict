# Read a string:
# s = input()
# Print a value:
# print(s)
diccionario = {}
for _ in range(int(input())):
  ingre, *latin = input().replace('- ', '').replace(',','').split()
  for palabra in latin:
    if palabra not in diccionario.keys():
      diccionario[palabra] = []
    diccionario[palabra].append(ingre)

print()
print(len(diccionario))
for i in sorted(diccionario.keys()):
  print (i, '-', ', '.join(sorted(list(diccionario[i]))))